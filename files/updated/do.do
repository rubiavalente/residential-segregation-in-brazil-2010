********************************************************************************
****************** Residential Segregation *************************************
********************************************************************************
** In order to create the dissimilarity indexes, you must first destring the 
** variables to transform them into numeric form, otherwise you won't be able to
** do any calculation.                                                 
** The data files are saved in the updated folder.
********************************************************************************
********************************************************************************

**************************************Manaus************************************
clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\manaus.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)


*******************************BELEM *******************************************
clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\belem.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)


**************************************Macapa ***********************************
clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\macapa.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

*************************************** NORTH **********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\macapa.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\belem.dta 
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\manaus.dta

save C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\north.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

*************************************Recife ***********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\recife.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

*******************************Salvador *********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\salvador.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

*****************************Fortaleza **********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\fortaleza.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*****************************Joao Pessoa **********************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\joaopessoa.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

**************************** Natal *****************************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\natal.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Teresina *********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\teresina.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


********************************Maceio ************************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\maceio.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


******************************Aracaju ***************************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\aracaju.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


********************************Sao Luis*************************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saoluis.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


********************************Feira de Santana *****************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\feiradesantana.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*******************************Campina Grande ******************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campinagrande.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

*******************************Itabuna *************************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\itabuna.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


********************************Northeast **********************************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\recife.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\salvador.dta 
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\fortaleza.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\joaopessoa.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\natal.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\teresina.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\maceio.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\aracaju.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saoluis.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\feiradesantana.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campinagrande.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\itabuna.dta

save C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\northeast.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*****************************Vale do Rio Cuiaba 
clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\cuiaba.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


******************************Goiania

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\goiania.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*******************************Brasilia

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\brasilia.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite



***************************** Central-West



clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\cuiaba.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\goiania.dta 
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\brasilia.dta

save C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\centralwest.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Rio de Janeiro


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\rio.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


********************************Belo Horizonte 


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\belohorizonte.dta

destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Vitoria


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\vitoria.dta

destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************BMA/V Redonda


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\barravolta.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Juiz de Fora 


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\juizdefora.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite
*********************************Ipatinga


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\ipatinga.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

*********************************Uberlandia


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\uberlandia.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Campos ***********


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campos.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*************************************Sao Paulo *********************************

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saopaulocapital.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

***************************Santos 


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\santos.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

***************************Campinas


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campinas.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

***************************Sao Jose dos Campos


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saojosedoscampos.dta

destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

***************************Sorocaba

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\sorocaba.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

****************************Ribeirao Preto

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\ribeirao.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

****************************Jundiai *****************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\jundiai.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

*****************************Southeast ******************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\rio.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\belohorizonte.dta 
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\vitoria.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\barravolta.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\juizdefora.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\ipatinga.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\uberlandia.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campos.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saopaulocapital.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\santos.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\campinas.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\saojosedoscampos.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\sorocaba.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\ribeirao.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\jundiai.dta


save C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\southeast.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

******************************Porto Alegre

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\portoalegre.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

********************************Curitiba

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\curitiba.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


******************************Pelotas

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\pelotas.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

******************************Florianopolis

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\florianopolis.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

******************************Londrina

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\londrina.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

******************************Joinville

clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\joinville.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite

******************************Caxias


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\caxias.dta
destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite


*****************************South ******************


clear

use C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\portoalegre.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\curitiba.dta 
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\pelotas.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\florianopolis.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\londrina.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\joinville.dta
append using C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\caxias.dta


save C:\Users\rubia.LENOVO-PC\Desktop\Segregation\updated\south.dta


destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

destring yellow, generate(amarelo) force
drop yello
rename amarelo yellow

destring indio, generate(indiobra) force
drop indio
rename indiobra indio

egen sumindio =sum(indio)

egen sumyellow =sum(yellow)

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

tab percentwhite





