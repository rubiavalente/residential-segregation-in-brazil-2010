********************************************************************
***************** Residential Segregation Project ******************
********************************************************************
**** Rubia Valente
*****************************
/***************************/
/* My usual stata commands */
/***************************/
clear all 
set more off
*version 11.2 
*Stata MP Parallel Edition

*Project folder

***Set local working directory where you want files stored
*** If you don't complete this step the do-file will not operate correctly
***Change path after "=" to your desired local path
	*Set working directory
	global working="C:\Users\rxv034000\Desktop\Segregation"
	*global working="Enter your path here"
	cd "$working"
	*Create subdirectories for data and output
	cap mkdir rawdata
	cap mkdir workingfiles
	
*Define macros for project directories
global rawdata=("$working"+"/rawdata")
global workingfiles=("$working"+"/workingfiles")

/*********************/
/* Accessing dataset */
/*******************************************************************************************************************/
* Use STATA transfer to get cvs files into dta.
/*******************************************************************************************************************/
* Note:  original data can be downloaded from: 
* IBGE  ftp://ftp.ibge.gov.br/Censos/Censo_Demografico_2010/Resultados_do_Universo/Agregados_por_Setores_Censitarios/
* I will be analyzing 35 metropolitan areas with population over 350.000 in 2010
********************************************************************************************************************/

*****************************************
************Sao Paulo Capital ***********
*****************************************
****** Metropolitan code id = 20 ********
*****************************************
clear
use "rawdata/Basico_SP1.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basico.dta", replace

clear

use "rawdata/Pessoa03_SP1.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006
*V001 everybody
*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoa.dta", replace


clear
use "workingfiles/basico.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoa.dta"

drop _merge

save "workingfiles/spcapital.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	destring `x', generate(`x'2)
	}
}

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\workingfiles\spcapital.dta"


****
**** This dataset is all of the S�o Paulo metropolitan area (capital)
**** so no need to drop any census tract, just drop strings...

drop V006 V005 V004 V003 V002 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/spcapital.dta", replace

rename V001 all
rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

use "workingfiles/spcapital.dta"

**** creating index of dissimilarity *****
****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

gen percentblack=100*(sumblack/sumraces)

gen percentbrown=100*(sumbrown/sumraces)



save "workingfiles/spcapital.dta", replace

****************************************************************
*****************************************
************Sao Paulo Interior  ***********
*****************************************
** Micro Codes
** Santos   = 35063
** Campinas = 35032					   
** Sao Jose dos Campos = 35050
** Sorocaba = 35046
** Ribeirao Preto = 35014 
** Jundiai = 35047
*****************************************


use "rawdata/Basico_SP2.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicosp2.dta", replace

clear

use "rawdata/Pessoa03_SP2.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoasp2.dta", replace


clear
use "workingfiles/basicosp2.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoasp2.dta"

drop _merge

save "workingfiles/spinterior.dta"


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

****


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/spinterior.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

use "workingfiles/spinterior.dta", replace

*****SANTOS *********

use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35063

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/santos.dta", replace


**********Campinas***********************


clear
use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35032

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/campinas.dta", replace

****************Sao Jose dos Campos **********

clear
use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35050

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/saojosedoscampos.dta", replace


***************Sorocaba***************************


clear
use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35046

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/sorocaba.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

***************Ribeirao Preto ***********************


clear
use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35014

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/ribeirao.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

**************Jundiai************************


clear
use "workingfiles/spinterior.dta", replace

drop if Cod_micro!=35047

**** creating index of dissimilarity *****
****
egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/jundiai.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


****************************************************************
*****************************************
************ Sul - South  ***************
*****************************************
************** Rio Grande do Sul ********
** Cod_RM
** Porto Alegre = 43026
** Caxias do Sul = 43016
** Pelotas = 43033
*****************************************

clear

use "rawdata/Basico_RS.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicors.dta", replace

clear

use "rawdata/Pessoa03_RS.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoars.dta", replace


clear
use "workingfiles/basicors.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoars.dta"

drop _merge

save "workingfiles/riograndedosul.dta"


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/riograndedosul.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/riograndedosul.dta", replace

****** Porto Alegre ********************

clear
use "workingfiles/riograndedosul.dta", replace

drop if Cod_RM!=34

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/portoalegre.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

***** Pelotas ***************

clear
use "workingfiles/riograndedosul.dta", replace

drop if Cod_micro!=43033

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/pelotas.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

********** Caxias de Sul ***************


clear
use "workingfiles/riograndedosul.dta", replace

drop if Cod_micro!=43016

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/caxias.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


****************************************************************
*****************************************
************ Sul - South  ***************
*****************************************
************** Santa Catarina ********
** Florianopolis
** Joinville 
*****************************************

clear

use "rawdata/Basico_SC.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicosc.dta", replace

clear

use "rawdata/Pessoa03_SC.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoasc.dta", replace


clear
use "workingfiles/basicosc.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoasc.dta"

drop _merge

save "workingfiles/santacatarina.dta"


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

****

drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/santacatarina.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/santacatarina.dta", replace

****** Florianopolis ********************

clear
use "workingfiles/santacatarina.dta", replace

drop if Cod_RM!=26

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/florianopolis.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


****** Joinville********************

clear
use "workingfiles/santacatarina.dta", replace

drop if Cod_micro!=42008

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/joinville.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


*****************************************
**************Paran� ******************
***************************************
*** Curitiba
*** Londrina
****************************************

clear

use "rawdata/Basico_PR.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicopr.dta", replace

clear

use "rawdata/Pessoa03_PR.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoapr.dta", replace


clear
use "workingfiles/basicopr.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoapr.dta"

drop _merge

save "workingfiles/parana.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

****

drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/parana.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/parana.dta", replace

****** Curitiba  ********************

clear
use "workingfiles/parana.dta", replace

drop if Cod_RM!=23

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/curitiba.dta", replace


tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


****** Londrina ********************

clear
use "workingfiles/parana.dta", replace

drop if Cod_RM!=24

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/londrina.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

***************************************************
************* Rio de Janeiro **********************
***************************************************
************* Rio de Janeiro
************* Barra-Mansa/Volta Redonda 
************* Campos (dos Goytacazes)
***************************************************

clear

use "rawdata/Basico_RJ.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicorj.dta", replace

clear

use "rawdata/Pessoa03_RJ.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoarj.dta", replace


clear
use "workingfiles/basicorj.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoarj.dta"

drop _merge

save "workingfiles/riodejaneiro.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/riodejaneiro.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/riodejaneiro.dta", replace


************* Rio de Janeiro

clear
use "workingfiles/riodejaneiro.dta", replace

tab Cod_RM Nome_da_RM2

drop if Cod_RM!=19


**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/rio.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

************* Barra-Mansa/Volta Redonda 
clear
use "workingfiles/riodejaneiro.dta", replace

tab Cod_RM Nome_da_RM2

keep if Nome_do_municipio2==92 | Nome_do_municipio2==8


**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/barravolta.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

************* Campos (dos Goytacazes)

clear
use "workingfiles/riodejaneiro.dta", replace


keep if Nome_da_micro2==4


**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/campos.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


**************************************************
*************** Minas Gerais *********************
**************************************************
*************** Belo Horizonte
*************** Juiz de Fora
*************** Ipatinga
*************** Uberlandia
***************************************************

clear

use "rawdata/Basico_MG.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicomg.dta", replace

clear

use "rawdata/Pessoa03_MG.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoamg.dta", replace


clear
use "workingfiles/basicomg.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoamg.dta"

drop _merge

save "workingfiles/minasgerais.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/minasgerais.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/minasgerais.dta", replace

*************** Belo Horizonte

clear
use "workingfiles/minasgerais.dta", replace

drop if Nome_da_meso2!=4

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/belohorizonte.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite

*************** Juiz de Fora

clear
use "workingfiles/minasgerais.dta", replace

tab 

drop if Nome_da_micro2!=32

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/juizdefora.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite



*************** Ipatinga

clear
use "workingfiles/minasgerais.dta", replace

tab 

drop if Nome_da_micro2!=25

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/ipatinga.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


*************** Uberlandia

clear
use "workingfiles/minasgerais.dta", replace

tab 

drop if Nome_da_micro2!=62

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/uberlandia.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite


*************** Espirito Santo********************
*************** Vitoria 
**************************************************

clear

use "rawdata/Basico_ES.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoes.dta", replace

clear

use "rawdata/Pessoa03_ES.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoaes.dta", replace


clear
use "workingfiles/basicoes.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoaes.dta"

drop _merge

save "workingfiles/espiritosanto.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/espiritosanto.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/espiritosanto.dta", replace


**************vitoria*************
clear
use "workingfiles/espiritosanto.dta", replace

tab 

drop if Nome_da_micro2!=13

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/vitoria.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

****************GOIAS*******************
****************************************
************bRASILIA
************Goiania
clear

use "rawdata/Basico_GO.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicogo.dta", replace

clear

use "rawdata/Pessoa03_GO.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoago.dta", replace


clear
use "workingfiles/basicogo.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoago.dta"

drop _merge

save "workingfiles/goias.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/goias.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/goias.dta", replace


**************Brasilia*************
clear
use "workingfiles/goias.dta", replace

tab 

drop if Nome_da_micro2!=7

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/brasilia.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

********goiania***********

clear
use "workingfiles/goias.dta", replace

tab 

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/goiania.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

********************************
**************Mato Grosso*******
*************************

clear

use "rawdata/Basico_MT.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicomt.dta", replace

clear

use "rawdata/Pessoa03_MT.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoamt.dta", replace


clear
use "workingfiles/basicomt.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoamt.dta"

drop _merge

save "workingfiles/matogrosso.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/matogrosso.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/matogrosso.dta", replace


**************Vale do Cuiaba*************
clear
use "workingfiles/matogrosso.dta", replace

tab 

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/valedoriocuiaba.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

*******Pernambuco PE
****** Recife

clear

use "rawdata/Basico_PE.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicope.dta", replace

clear

use "rawdata/Pessoa03_PE.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoape.dta", replace


clear
use "workingfiles/basicope.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoape.dta"

drop _merge

save "workingfiles/pernambuco.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/pernambuco.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/pernambuco.dta", replace


**************Recife*************
clear
use "workingfiles/pernambuco.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/recife.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 


*******Bahia BA***********************
****Salvador
****Feira de Santana
****Itabuna
*******************************
clear

use "rawdata/Basico_BA.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoba.dta", replace

clear

use "rawdata/Pessoa03_BA.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoaba.dta", replace


clear
use "workingfiles/basicoba.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoaba.dta"

drop _merge

save "workingfiles/bahia.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/bahia.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/bahia.dta", replace


**************Salvador*************
clear
use "workingfiles/bahia.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/salvador.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 


****Feira de Santana

clear
use "workingfiles/bahia.dta", replace

tab Nome_da_micro2 
tab Nome_da_micro2, nola

drop if Nome_da_micro2!=11

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/feiradesantana.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

****Itabuna

clear
use "workingfiles/bahia.dta", replace

tab Nome_da_micro2 
tab Nome_da_micro2, nola

drop if Nome_da_micro2!=13

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/itabuna.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

***************************************
*******Ceara CE
***Fortaleza
***************************************
clear

use "rawdata/Basico_CE.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoce.dta", replace

clear

use "rawdata/Pessoa03_CE.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoace.dta", replace


clear
use "workingfiles/basicoce.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoace.dta"

drop _merge

save "workingfiles/ceara.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/ceara.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/ceara.dta", replace


*************fortaleza******************

clear
use "workingfiles/ceara.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/fortaleza.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

*******************************************
*******Paraiba PB
***Joao Pessoa
***Campina Grande
*******************************************
clear

use "rawdata/Basico_PB.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicopb.dta", replace

clear

use "rawdata/Pessoa03_PB.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoapb.dta", replace


clear
use "workingfiles/basicopb.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoapb.dta"

drop _merge

save "workingfiles/paraiba.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/paraiba.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/paraiba.dta", replace


*************joao pessoa******************

clear
use "workingfiles/paraiba.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/joaopessoa.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

*************campina grande******************

clear
use "workingfiles/paraiba.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/campinagrande.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

********************************
*******Rio Grande do Norte RN
*** Natal
********************************
clear

use "rawdata/Basico_RN.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicorn.dta", replace

clear

use "rawdata/Pessoa03_RN.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoarn.dta", replace


clear
use "workingfiles/basicorn.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoarn.dta"

drop _merge

save "workingfiles/riograndedonorte.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/riograndedonorte.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/riograndedonorte.dta", replace


*************natal******************

clear
use "workingfiles/riograndedonorte.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/natal.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

************************
*******Piaui PI
*** Teresina
************************

clear

use "rawdata/Basico_PI.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicopi.dta", replace

clear

use "rawdata/Pessoa03_PI.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoapi.dta", replace


clear
use "workingfiles/basicopi.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoapi.dta"

drop _merge

save "workingfiles/piaui.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/piaui.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/piaui.dta", replace


*************teresina******************

clear
use "workingfiles/piaui.dta", replace

tab Nome_da_micro2 
tab Nome_da_micro2, nola

drop if Nome_da_micro2!=14

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/teresina.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 


**************************************
*******Alagoas AL
*** Maceio
**************************************

clear

use "rawdata/Basico_AL.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoal.dta", replace

clear

use "rawdata/Pessoa03_AL.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoaal.dta", replace


clear
use "workingfiles/basicoal.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoaal.dta"

drop _merge

save "workingfiles/alagoas.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/alagoas.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/alagoas.dta", replace



*************maceio******************

clear
use "workingfiles/alagoas.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/maceio.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

************************************
*******Sergipe SE
*** Aracaju
************************************

clear

use "rawdata/Basico_SE.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicose.dta", replace

clear

use "rawdata/Pessoa03_SE.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoase.dta", replace


clear
use "workingfiles/basicose.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoase.dta"

drop _merge

save "workingfiles/sergipe.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/sergipe.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/sergipe.dta", replace


*************aracaju******************

clear
use "workingfiles/sergipe.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/aracaju.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

*********************************
*******Maranhao MA
***** Sao Luis
********************************

clear

use "rawdata/Basico_MA.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoma.dta", replace

clear

use "rawdata/Pessoa03_MA.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoama.dta", replace


clear
use "workingfiles/basicoma.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoama.dta"

drop _merge

save "workingfiles/maranhao.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/maranhao.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/maranhao.dta", replace


*************sao luis******************

clear
use "workingfiles/maranhao.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=3

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/saoluis.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 


*******************************
*******Amazonas AM
***Manaus
********************************

clear

use "rawdata/Basico_AM.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoam.dta", replace

clear

use "rawdata/Pessoa03_AM.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoaam.dta", replace


clear
use "workingfiles/basicoam.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoaam.dta"

drop _merge

save "workingfiles/amazonas.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/amazonas.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/amazonas.dta", replace


*************manaus******************

clear
use "workingfiles/amazonas.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/manaus.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

*************************
*******Para PA
***Belem
**********************

clear

use "rawdata/Basico_PA.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicopa.dta", replace

clear

use "rawdata/Pessoa03_PA.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoapa.dta", replace


clear
use "workingfiles/basicopa.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoapa.dta"

drop _merge

save "workingfiles/para.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/para.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/para.dta", replace


*************belem******************

clear
use "workingfiles/para.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/belem.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 

************************
*******Amapa AP
***Macapa
************************


clear

use "rawdata/Basico_AP.dta"

drop V001 V002 V003 V004 V005 V006 V007 V008 V009 V010 V011 V012

save "workingfiles/basicoap.dta", replace

clear

use "rawdata/Pessoa03_AP.dta"

keep Cod_setor Situacao_setor V001 V002 V003 V004 V005 V006

*V002 White residents
*V003 Black residents
*V004 Yellow residents
*V005 Brown residents
*V006 Indigenous residents

save "workingfiles/pessoaap.dta", replace


clear
use "workingfiles/basicoap.dta"

merge m:m Cod_setor Situacao_setor using "workingfiles/pessoaap.dta"

drop _merge

save "workingfiles/amapa.dta", replace


*** we need to destring variables

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}


drop V006 V005 V004 V003 V002 V001 Tipo_setor Situacao_setor Nome_Grande_Regiao Nome_da_UF Nome_da_meso Nome_da_micro Nome_da_RM Nome_do_municipio Nome_do_distrito Nome_do_subdistrito Nome_do_bairro

save "workingfiles/amapa.dta", replace

rename V0022 white
rename V0032 black
rename V0042 yellow
rename V0052 brown
rename V0062 indio

save "workingfiles/amapa.dta", replace


*************macapa******************

clear
use "workingfiles/amapa.dta", replace

tab Nome_da_RM2 
tab Nome_da_RM2, nola

drop if Nome_da_RM2!=2

**** creating index of dissimilarity *****

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumyellow =sum(yellow)

egen sumbrown =sum(brown)

egen sumindio =sum(indio)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

gen sumraces=sumwhite+sumblack+sumyellow+sumbrown+sumindio

gen percentwhite=100*(sumwhite/sumraces)

save "workingfiles/macapa.dta", replace

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack
tab percentwhite 


