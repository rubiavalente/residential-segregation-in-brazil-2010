*****Household Segregation paper - income **********
*** this data was extracted from http://www.sidra.ibge.gov.br/bda/tabela/listabl.asp?c=1385&z=cd&o=7
*** looking at the results for Tabela 1385 - 	Pessoas de 10 anos ou mais de idade, respons�veis pelos domic�lios particulares, 
*** por cor ou ra�a, segundo o sexo e as classes de rendimento nominal mensal domiciliar per capita
***
***Nivel Territoriais, data at the bairro level for Feira de Santana, Rio de Janeiro, Belo Horizonte, Barra-Mansa Volta Redonda, Juiz de Fora and Campos
***select the state for each of this regions in the "bairro(14210) category), everything select nao"
***Nivel Territoriais, subdistrito(658) Bahia(27) data at the municipal level for Salvador; only data available
***Sao Jose dos Campos by socioeconomico setor/bairros?

*******************************************************************************************************
** INCOME: Unidade Territorial, Nives territoriais, Bairro, em Regiao Metropolitana

** Not all bairros were available, only used the ones that were available in the IBGE website. 


/***************************/
/* My usual stata commands */
/***************************/
clear all 
set more off
*version 11.2 
*Stata MP Parallel Edition

*Project folder

***Set local working directory where you want files stored
*** If you don't complete this step the do-file will not operate correctly
***Change path after "=" to your desired local path
	*Set working directory
	global working="C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income"
	*global working="Enter your path here"
	cd "$working"
	*Create subdirectories for data and output
	cap mkdir rawdata
	cap mkdir workingfiles
	
*Define macros for project directories
global rawdata=("$working"+"/rawdata")
global workingfiles=("$working"+"/workingfiles")

*********************************************************************
************ Feira de Santana ***************************************
**************** no income and less than 1 **************************
clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantanaless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

*check if # of people in groups is at least TWICE the census track

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1526
*whitebrown .0933
*brownblack .0776

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1689
*whitebrown .0849
*brownblack .0975

*******************************2 to 3 mw *****************************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana2to3.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

destring black, generate(negro) force
drop black
rename negro black

destring white, generate(branco)
drop white
rename branco white



egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .203
*whitebrown .119
*brownblack .120

**************************** 3 to 5 mw ****************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana3to5.dta"

rename var1 bairros

drop var3 var5

destring black, generate(negro)

drop black
rename negro black

destring white, generate(branco)
drop white
rename branco white


destring brown, generate(pardo)
drop brown
rename pardo brown


egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .221
*whitebrown .104
*brownblack .151 


******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .288
*whitebrown .136
*brownblack .202

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantanamore10.dta"


rename var1 bairros
drop var3 var5


*double check if it's all the same neighborhoods


destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack no enough blacks
*whitebrown .3058
*brownblack no enough blacks 67, needed 88 



*********************************************************************
************ Rio de Janeiro   ***************************************
**************** no income and less than 1 **************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rioless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .138
*whitebrown .124
*brownblack .093

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white


destring brown, generate(pardo) force
drop brown
rename pardo brown

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .166
*whitebrown .167
*brownblack .075


****************************** 2 to 3 mw *************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio2to3.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .253
*whitebrown .225
*brownblack .076 

******************************* 3 to 5 mw *************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio3to5.dta"



rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .326
*whitebrown .269
*brownblack .094


******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio5to10.dta"



rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .374
*whitebrown .287
*brownblack .120

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riomore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .349
*whitebrown .259
*brownblack .135




*********************************************************************
************ Belo Horizonte   ***************************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonteless1.dta"

*double check if it's all the same neighborhoods

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********



destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .225
*whitebrown .177
*brownblack .101

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte1to2.dta"




destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .268
*whitebrown .198
*brownblack .109

****************************2 to 3 mw ***********************


clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte2to3.dta"

rename var1 bairros
drop var3 var5

destring black, generate(negro) force

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .339
* .233
* .156

**************************3 to 5 mw **************************

clear

use

save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte3to5.dta", replace


rename var1 bairros

rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack


*.352
*.246
*.166



******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte5to10.dta"


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .411
*whitebrown .262
*brownblack .210

******************** more than 10 *********************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizontemore10.dta"



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack  needed 962 blacks only 575
*whitebrown  .267
*brownblack 


*********************************************************************
************ Barra Mansa - Volta Redonda ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravoltaless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .162
*whitebrown .128
*brownblack .102 

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravolta1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .194
*whitebrown .154 
*brownblack .112

****************************** 2 to 3 mw ******************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravolta2to3.dta"

rename var1 bairros

drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*.266
*.198
*.155

****************************** 3 to 5 mw ******************************


clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravolta3to5.dta"

rename var1 bairros
drop var3 var5

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*.332
*.246
*.214



******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravolta5to10.dta"


rename var1 bairros

drop var3 var5



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*needed 280 blacks, only 176 
*whitebrown .278

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\barravoltamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown



*double check if it's all the same neighborhoods



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*indexes can't be calculated if either of the two color groups in an 
*income bracket is not at least twice the number of censustracks/bairros
*cite Doug Massey
 
*both black and brown are less than 280, blacks were 36, brown 11



*********************************************************************
************ Juiz de Fora 				 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juizless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .200
*whitebrown .133
*brownblack .108

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5



destring black, generate(negro) force 

drop black
rename negro black

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .249
*whitebrown .185
*brownblack .114 


*********************************2 to 3 mw*************************



clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz2to3.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack


* .345
* .230
* .161



******************************* 3 to 5 mw **************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz3to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .392
* .240
* .216


******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .336
*whitebrown .222
*brownblack .237

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juizmore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*only 57 blacks, can only calculate index for whitebrown:

*whiteblack 
*whitebrown .279 
*brownblack  



*********************************************************************
************ Campos						 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\camposless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2268513
*whitebrown .1658525
*brownblack .110672

********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2681203
*whitebrown .1983816
*brownblack .1498418


******************************** 2 to 3 mw ***********************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos2to3.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .313
* .209
* .199


******************************* 3 to 5 mw **************************


clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos3to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .321
* .202
* .221


******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whitebrown .212
*not enough blacks, only 142, needed 156 

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\camposmore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*not enough blacks to calculate index, only 34
*whiteblack 
*whitebrown .236
*brownblack 




*********************************************************************
************ Salvador     **********	 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvadorless1.dta"

rename var1 subdistrict

*double check if it's all the same neighborhoods
drop var3 var5

 



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .0510156
*whitebrown .061519
*brownblack .0298307
********************* 1 to 2 minimum wage

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvador1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1550971
*whitebrown .1371486
*brownblack .0322731 

********************* 2 to 3 mw ***************************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvador2to3.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5



egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2929224
*whitebrown .1970139
*brownblack .1022597



******************* 3 to 5 mw***********************

clear
use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvador3to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


destring white, generate(branco) force
drop white
rename branco white



egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .332
*whitebrown .189
*brownblack .148





******************** 5 to 10 *******************
clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvador5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3255573
*whitebrown .1490572
*brownblack .1801404

******************** more than 10 

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\salvadormore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5



destring white, generate(branco) force
drop white
rename branco white




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .276
*whitebrown .103
*brownblack .173


****************************Manaus ********************
*******************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manausless1.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack    .112 
*white brown  .077299 
*brownblack  .097 


********1 to 2***********************************************************



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus1to2.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack    .144
*whitebrown  .089 
*brownblack  .107 


****************2 to 3 ****************************************************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus2to3.dta"
rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack  .193
*whitebrown .120
*brownblack .125

*********************3 to 5***********************************************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus3to5.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .205
*whitebrown .154
*brownblack .161

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus5to10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*tab indexwhiteblack .272
*tab indexwhitebrown .171
*tab indexbrownblack .173

******************************more than 10**********************************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manausmore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whitebrown  .138

*no enough blacks only 177, needed 244
****************************BELEM  ********************
*******************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belemless1.dta"




rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack .098987 
tab indexwhitebrown .0745273 
tab indexbrownblack .0654105 



********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem1to2.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white







**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack  


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem2to3.dta"




rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack  

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack 

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belemmore10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack  

**************************MACAPA**********************************

******************************************************************
******************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack .1349431 
tab indexwhitebrown .0627941 
tab indexbrownblack .1284181 

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  .1394666 
tab indexwhitebrown  .0779124 
tab indexbrownblack  .106481 


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack .139
tab indexwhitebrown .080 
tab indexbrownblack .119

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack .149 
tab indexwhitebrown .106 
tab indexbrownblack .113 

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack .196 
tab indexwhitebrown .134 
tab indexbrownblack .151 

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  need 82 blacks, only 68
tab indexwhitebrown  
tab indexbrownblack  

*********************************************************************
************************ RECIFE *************************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recifeless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown  
tab indexbrownblack 

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recifemore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    .118
tab indexbrownblack    need 548 blacks only 367 


*********************************************************************
************************FORTALEZA ***********************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortalezaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack  


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortalezamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)


tab indexwhitebrown    
*not enough blacks 192 only, needed 574  


*********************************************************************
********************JOAO PESSOA *************************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack 

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack 

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  *not enough blacks
tab indexwhitebrown   
tab indexbrownblack   *not enough blacks


*********************************************************************
********************* NATAL ****************************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natalless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack 

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 
drop black
rename negro black






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  


******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natalmore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown   
tab indexbrownblack  


*********************************************************************
********************** TERESINA *************************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresinaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown 
tab indexbrownblack  

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack 

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  
***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresinamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack  note enough blacks only 91, needed 224
tab indexwhitebrown  
*tab indexbrownblack  not enough blacks

*********************************************************************
********************** MACEIO ***************************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceioless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  .0981802 
tab indexwhitebrown  .0655394 
tab indexbrownblack  .0730645 

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   


****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown  
tab indexbrownblack  

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown 
tab indexbrownblack 

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceiomore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack  *not enough blacks again...neede 118, only had 117
tab indexwhitebrown   
*tab indexbrownblack    

*********************************************************************
********************* ARACAJU ***************************************
*********************************************************************



***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracajuless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  .110207 
tab indexwhitebrown  .0871759 
tab indexbrownblack  .0675625 

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   .1900161
tab indexwhitebrown   .1184937 
tab indexbrownblack   .092318 

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   .220
tab indexwhitebrown   .152 
tab indexbrownblack   .103

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracajumore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*****************************************************************
******************* CAMPINA GRANDE ******************************
*****************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrandeless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   .181
tab indexwhitebrown   .083
tab indexbrownblack   .144

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   note enough blacks only 93, needed at least 100
tab indexwhitebrown  
tab indexbrownblack   

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrandemore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack    
tab indexwhitebrown    
*tab indexbrownblack 71 blacks, needed 100 blacks 

*****************************************************************
********************* Vale do Rio Cuiaba ************************
*****************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiabaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack    

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiabamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks
tab indexwhitebrown    
tab indexbrownblack    

*****************************************************************
********************* Goiania ***********************************
*****************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goianialess1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goiania1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goiania2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goiania3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goiania5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\goianiamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack   


******************************************************************
********************* Brasilia ************************************
******************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasilialess1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasilia1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   .103
tab indexwhitebrown   .090
tab indexbrownblack   .033

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasilia2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    .158
tab indexwhitebrown    .131
tab indexbrownblack    .047

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasilia3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  .199
tab indexwhitebrown   .140
tab indexbrownblack   .065

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasilia5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   .233
tab indexwhitebrown  .195
tab indexbrownblack  .061

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\brasiliamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    .237
tab indexwhitebrown    .157
tab indexbrownblack   .086

*******************************************************************
**********************Vitoria ************************************
********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitorialess1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack    

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown   
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoriamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack    not enough blacks - only 146, needed 164
tab indexwhitebrown    
*tab indexbrownblack   


******************************************************************
********************** Ipatinga ***********************************
*****************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatingaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack  

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatingamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks needed at least 62, had 13 only
tab indexwhitebrown   
tab indexbrownblack    


********************************************************************
************************** Uberlandia ******************************
********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandialess1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown   
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandiamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks only 53, needed 102
tab indexwhitebrown    
tab indexbrownblack   


********************************************************************
*********************** Sao Paulo ***********************************
*********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopauloless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  
******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulomore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 307 blacks only needed 918  
tab indexwhitebrown    
tab indexbrownblack   

********************************************************************
************************** SANTOS **********************************
********************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santosless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santos1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santos2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown   
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santos3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santos5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\santosmore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack   .461 needed 440 blacks, only 128
tab indexwhitebrown   
*tab indexbrownblack   .403

*******************************************************************
*********************** CAMPINAS **********************************
*******************************************************************


***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinasless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack    

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack 
tab indexwhitebrown  
tab indexbrownblack  

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack   

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown  
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinasmore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

*tab indexwhiteblack   not enough blacks and brown
tab indexwhitebrown    
*tab indexbrownblack   

*******************************************************************
********************* SAO Jose dos Campos *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojoseless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojosemore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks needed 70 only had 59
tab indexwhitebrown    
tab indexbrownblack   



**************************************************************************
********************* SOROCABA            ********************************
**************************************************************************
********Bairro/Em Regiao Metropolitana - Data for Tatui and Boituva only *



***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocabaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

not enough blacks needed 292, only 109
tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocabamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   


*******************************************************************
********************* RIBEIRAO PRETO     *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberaoless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods



destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberaomore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack   


*******************************************************************
********************* Jundiai *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiailess1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiaimore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   



*******************************************************************
********************* PORTO ALEGRE *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegreless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegremore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack   


*******************************************************************
********************* Curitiba     *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitibaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitibamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
tab indexwhitebrown    
tab indexbrownblack 
*not enough blacks  

*******************************************************************
********************* Pelotas/ Rio Grande RS *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasrioless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasrio1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasrio2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasrio3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasrio5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\pelotasriomore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
*not enough blacks or browns 
tab indexwhitebrown    
tab indexbrownblack   


*******************************************************************
********************* Florianopolis       *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolisless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolismore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
 *not enough blacks
tab indexwhitebrown    
tab indexbrownblack   

*******************************************************************
********************* Londrina            *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrinaless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black






**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 

drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrinamore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    
*not enough blacks needed 70 only had 59
tab indexwhitebrown    
tab indexbrownblack   


*******************************************************************
********************* Joinville           *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinvilleless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 
drop black
rename negro black

destring brown, generate(pardo) force
drop brown
rename pardo brown




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 

drop black
rename negro black

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 
drop black
rename negro black


destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinvillemore10.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks or browns 
tab indexwhitebrown    
tab indexbrownblack   

*******************************************************************
********************* Caixias  do Sul     *************************
*******************************************************************

***less 1


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxiasless1.dta"



rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods

destring black, generate(negro) force 

drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias1to2.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods
destring black, generate(negro) force 
drop black
rename negro black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

****************2 to 3 ***********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias2to3.dta"


rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

*double check if it's all the same neighborhoods


destring black, generate(negro) force 
drop black
rename negro black



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack    

*********************3 to 5************ 


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias3to5.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 
drop black
rename negro black

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias5to10.dta"

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown


destring black, generate(negro) force 

drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10

rename var1 bairros
rename var2 white
rename var3 black
rename var4 brown

destring black, generate(negro) force 
drop black
rename negro black

destring white, generate(branco) force
drop white
rename branco white

destring brown, generate(pardo) force
drop brown
rename pardo brown


**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack    not enough blacks needed 70 only had 59
tab indexwhitebrown    
tab indexbrownblack   


***************************************************************************
********************* Combined Index **************************************
***************************************************************************
******only merge cities with bairros level data ***************************
***** this does not include salvador, goiania, brasilia and pelotas, which 
***** were at the sub district level ***************************************
****************************************************************************


clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxiasless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinvilleless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrinaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolisless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitibaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegreless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiailess1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberaoless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocabaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojoseless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinasless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopauloless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\camposless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandialess1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatingaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juizless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitorialess1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonteless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rioless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiabaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrandeless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantanaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracajuless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceioless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresinaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natalless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortalezaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recifeless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapaless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belemless1.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manausless1.dta"

***less 1



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combinedless1.dta"

drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

********1 to 2



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem1to2.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus1to2.dta"




save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combined1to2.dta"

drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   

****************2 to 3 ***********************



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem2to3.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus2to3.dta"



save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combined2to3.dta"

drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack


**** creating index of dissimilarity *********


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown   
tab indexbrownblack   
  

*********************3 to 5************ 



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem3to5.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus3to5.dta"



save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combined3to5.dta", replace

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combined3to5.dta"

drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack





**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

***********************5 to 10 ********************



clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxias5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinville5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrina5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolis5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitiba5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegre5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiai5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberao5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocaba5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojose5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinas5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulo5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campos5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandia5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatinga5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juiz5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoria5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizonte5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\rio5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiaba5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrande5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantana5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracaju5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceio5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresina5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natal5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoa5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortaleza5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recife5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapa5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belem5to10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manaus5to10.dta"




save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combined5to10.dta", replace



drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack  
tab indexwhitebrown   
tab indexbrownblack  

******************************more than 10

clear

use "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\caxiasmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joinvillemore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\londrinamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\florianopolismore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\curitibamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\portoalegremore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\jundiaimore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riberaomore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\sorocabamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saojosemore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinasmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\saopaulomore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\camposmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\uberlandiamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\ipatingamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\juizmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\vitoriamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belohorizontemore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\riomore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\cuiabamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\campinagrandemore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\feiradesantanamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\aracajumore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\maceiomore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\teresinamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\natalmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\joaopessoamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\fortalezamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\recifemore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\macapamore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\belemmore10.dta"
append using "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\manausmore10.dta"




save "C:\Users\rubia.LENOVO-PC\Desktop\Segregation\Income\combinedmore10.dta", replace


drop sumwhite sumblack sumbrown wi bi browni indexwhiteblack indexwhitebrown indexbrownblack




**** creating index of dissimilarity *********


egen sumwhite = sum(white)

egen sumblack = sum(black)

egen sumbrown = sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack   
tab indexwhitebrown    
tab indexbrownblack   

