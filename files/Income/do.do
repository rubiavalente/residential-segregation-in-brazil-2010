*****Household Segregation paper - income **********
*** this data was extracted from http://www.sidra.ibge.gov.br/bda/tabela/listabl.asp?c=1385&z=cd&o=7
*** looking at the results for Tabela 1385 - 	Pessoas de 10 anos ou mais de idade, respons�veis pelos domic�lios particulares, 
*** por cor ou ra�a, segundo o sexo e as classes de rendimento nominal mensal domiciliar per capita
***
***Nivel Territoriais, data at the bairro level for Feira de Santana, Rio de Janeiro, Belo Horizonte, Barra-Mansa Volta Redonda, Juiz de Fora and Campos
***select the state for each of this regions in the "bairro(14210) category), everything select nao"
***Nivel Territoriais, subdistrito(658) Bahia(27) data at the municipal level for Salvador; only data available
***Sao Jose dos Campos by socioeconomico setor/bairros?

*******************************************************************************************************
** INCOME: Unidade Territorial, Nives territoriais, Bairro, em Regiao Metropolitana

** Not all bairros were available, only used the ones that were available in the IBGE website. 


/***************************/
/* My usual stata commands */
/***************************/
clear all 
set more off
*version 11.2 
*Stata MP Parallel Edition

*Project folder

***Set local working directory where you want files stored
*** If you don't complete this step the do-file will not operate correctly
***Change path after "=" to your desired local path
	*Set working directory
	global working="C:\Users\rxv034000\Desktop\Segregation\Income"
	*global working="Enter your path here"
	cd "$working"
	*Create subdirectories for data and output
	cap mkdir rawdata
	cap mkdir workingfiles
	
*Define macros for project directories
global rawdata=("$working"+"/rawdata")
global workingfiles=("$working"+"/workingfiles")

*********************************************************************
************ Feira de Santana ***************************************
**************** no income and less than 1 **************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\feiradesantanaless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1526
*whitebrown .0933
*brownblack .0776

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\feiradesantana1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1689
*whitebrown .0849
*brownblack .0975

*******************************2 to 3 mw *****************************


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop black white

rename black2 black

rename white2 white

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3125
*whitebrown .4346
*brownblack .3301

**************************** 3 to 5 mw ****************************



foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop black white brown

rename black2 black

rename white2 white

rename brown2 brown

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3118
*whitebrown .2305
*brownblack .3846 



********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\feiradesantana2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop black white

rename black2 black

rename white2 white



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3754
*whitebrown .1062
*brownblack .3289

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\feiradesantana5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3458
*whitebrown .1707
*brownblack .3555

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\feiradesantanamore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3411
*whitebrown .3058
*brownblack .3853




*********************************************************************
************ Rio de Janeiro   ***************************************
**************** no income and less than 1 **************************

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\rioless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black brown

rename black2 black
rename brown2 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4849
*whitebrown .4352
*brownblack .4574

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\rio1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3699
*whitebrown .2692
*brownblack .4678


****************************** 2 to 3 mw *************************


drop black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3235
*whitebrown .3227
*brownblack .4532 

******************************* 3 to 5 mw *************************


drop black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3414
*whitebrown .3232
*brownblack .4527


********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\rio2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3388
*whitebrown .3408
*brownblack .4619

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\rio5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3177
*whitebrown .3777
*brownblack .4293

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\riomore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3640
*whitebrown .3908
*brownblack .4434




*********************************************************************
************ Belo Horizonte   ***************************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\belohorizonteless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3449
*whitebrown .3472
*brownblack .4485

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\belohorizonte1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3576
*whitebrown .3265
*brownblack .4313

****************************2 to 3 mw ***********************



drop black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .4311
* .3278
* .4822

**************************3 to 5 mw **************************

clear



drop black white brown

rename black2 black
rename brown2 brown
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack


*.4134
*.3086
*.4673



********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\belohorizonte2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3995404
*whitebrown .3313197
*brownblack .4438196

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\belohorizonte5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4453862
*whitebrown .3365217
*brownblack .4710139

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\belohorizontemore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .471977
*whitebrown  .3774041
*brownblack .4020056



*********************************************************************
************ Barra Mansa - Volta Redonda ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\barravoltaless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black

rename black2 black


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4845933
*whitebrown .1278028
*brownblack .5242034 

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\barravolta1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white 

rename black2 black
rename white2 white

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3666903
*whitebrown .5200265 
*brownblack .4337844

****************************** 2 to 3 mw ******************************

clear

drop black brown white 

rename black2 black
rename white2 white
rename brown2 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*.4045
*.3565
*.4174

****************************** 3 to 5 mw ******************************

clear 


drop black brown white 

rename black2 black
rename white2 white
rename brown2 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*.451
*.345
*.355





********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\barravolta2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3954381
*whitebrown .3511926
*brownblack .4198468

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\barravolta5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3996065
*whitebrown .4310027
*brownblack .3450647

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\barravoltamore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4478752
*whitebrown  .3838857
*brownblack .2862319 




*********************************************************************
************ Juiz de Fora 				 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\juizless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black

rename black2 black


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4043546
*whitebrown .1328705
*brownblack .4399821

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\juiz1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black 

rename black2 black


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4241333
*whitebrown .1847361
*brownblack .4382764 


*********************************2 to 3 mw*************************

clear


drop  black brown 

rename black2 black
rename brown2 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack


* .364
* .378
* .387



******************************* 3 to 5 mw **************************

clear


drop  black brown 

rename black2 black
rename brown2 brown

**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .488
* .441
* .366




********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\juiz2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black 

rename black2 black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4039834
*whitebrown .2418766
*brownblack .2697576

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\juiz5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .397367
*whitebrown .3294985
*brownblack .3984877

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\juizmore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .337963
*whitebrown .3899087 
*brownblack .3696079 



*********************************************************************
************ Campos						 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\camposless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2268513
*whitebrown .1658525
*brownblack .110672

********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\campos1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2681203
*whitebrown .1983816
*brownblack .1498418


******************************** 2 to 3 mw ***********************

clear


drop brown black

rename brown2 brown
rename black2 black 


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .389
* .404
* .407


******************************* 3 to 5 mw **************************

clear



drop brown black white

rename white2 white
rename brown2 brown
rename black2 black 


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

* .317
* .382
* .387




********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\campos2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black 

rename black2 black




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4480226
*whitebrown .2301016
*brownblack .4264731 

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\campos5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2904612
*whitebrown .3422741
*brownblack .3398318 

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\camposmore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 black white brown

rename black2 black
rename white2 white
rename brown2 brown


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .4393777
*whitebrown .3761809
*brownblack .3829246




*********************************************************************
************ Salvador     **********	 ****************************
**************** no income and less than 1 **************************

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\salvadorless1.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .0510156
*whitebrown .061519
*brownblack .0298307
********************* 1 to 2 minimum wage

clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\salvador1to2.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5


foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 


**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .1550971
*whitebrown .1371486
*brownblack .0322731 

********************* 2 to 3 mw ***************************


egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .2929224
*whitebrown .1970139
*brownblack .1022597



******************* 3 to 5 mw***********************


egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .5231863
*whitebrown .5549012
*brownblack .1480408




********************************2 to 5 mw ****************************
clear
use "C:\Users\rxv034000\Desktop\Segregation\Income\salvador2to5.dta"

rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3327683
*whitebrown .2070714
*brownblack .1267313

******************** 5 to 10 *******************
clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\salvador5to10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 




**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .3255573
*whitebrown .1490572
*brownblack .1801404

******************** more than 10 

clear

use "C:\Users\rxv034000\Desktop\Segregation\Income\salvadormore10.dta"


rename var1 bairros

*double check if it's all the same neighborhoods
drop var3 var5

foreach x of varlist * { 
	capture confirm string variable `x'
	if !_rc & "`x'" !="ID" {
	encode `x', generate(`x'2)
	}
}

drop bairros2 white
rename white2 white



**** creating index of dissimilarity *********

egen sumwhite =sum(white)

egen sumblack =sum(black)

egen sumbrown =sum(brown)

gen wi= (white/sumwhite)

gen bi= (black/sumblack)

gen browni= (brown/sumbrown)

egen indexwhiteblack=sum((abs(wi-bi))/2)

egen indexwhitebrown=sum((abs(wi-browni))/2)

egen indexbrownblack=sum((abs(browni-bi))/2)

tab indexwhiteblack
tab indexwhitebrown
tab indexbrownblack

*whiteblack .638212
*whitebrown .7559498
*brownblack .1732785



