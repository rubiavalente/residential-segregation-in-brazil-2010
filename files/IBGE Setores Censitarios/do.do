********************************************************************
***************** Residential Segregation Project ******************
********************************************************************
**** Rubia R. Valente
*****************************
/***************************/
/* My usual stata commands */
/***************************/
clear all 
set more off
*version 11.2 
*Stata MP Parallel Edition

*Project folder

***Set local working directory where you want files stored
*** If you don't complete this step the do-file will not operate correctly
***Change path after "=" to your desired local path
	*Set working directory
	global working="C:\Users\rxv034000\Desktop\Segregation"
	*global working="Enter your path here"
	cd "$working"
	*Create subdirectories for data and output
	cap mkdir rawdata
	cap mkdir workingfiles
	
*Define macros for project directories
global rawdata=("$working"+"/rawdata")
global workingfiles=("$working"+"/workingfiles")

/*********************/
/* Accessing dataset */
/*******************************************************************************************************************/
* Use STATA transfer to get cvs files into dta.
/*******************************************************************************************************************/
* Note:  original data can be downloaded from: 
* IBGE  ftp://ftp.ibge.gov.br/Censos/Censo_Demografico_2010/Resultados_do_Universo/Agregados_por_Setores_Censitarios/
* I will be analyzing 40 metropolitan areas with population over 350.000 in 2010
********************************************************************************************************************/




