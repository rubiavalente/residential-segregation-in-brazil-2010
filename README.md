# Residential Segregation in Brazil 2010

This folder contains the data/code used in the paper, Residential Segregation by Skin Color: Brazil Revisited, forthcoming at LARR 2020. 

The raw data downloaded from IBGE is included in the files under IBGE Setores Censitarios, although they can also be downloaded from IBGE's website:   

ftp://ftp.ibge.gov.br/Censos/Censo_Demografico_2010/Resultados_do_Universo/Agregados_por_Setores_Censitarios/

For replication, note that you must first destring the dataset before creating the indexes, refer to the _updated folder. 

All files and do files are included in the folders. 